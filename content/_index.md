---
title: Trang
banner: bison
quality: q33
bgcollab: appetizers
bgcollabq: q50
delimiter: ×
names:
- Tran
- Creator
- Photographer
- Scholar
- Fitness
stuff:
- reading
- gastronomy
- powerlifting
- cinema
- dancing
- lettering
- watercolor
- bujo
- traveling
- photography
---
From one artist's passion to another, captivating the chef's imagination through the lens of a vicarious *evocatrice*.

