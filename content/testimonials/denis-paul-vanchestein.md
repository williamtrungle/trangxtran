---
title: "Denis Paul Vanchestein"
date: 2019-05-02T11:49:18-04:00
---
"Saisir le moment, c'est essentiel en photographie; un don inné chez les photographes doués. Trang a le don du moment, elle sait créer des compositions qui nous interpellent et nous incitent à voir vraiment... plutôt que de simplement regarder. Qui plus est, d'un professionnalisme irréprochable et de bonne compagnie, travailler avec Trang est vraiment agréable."

**Denis Paul Vanchestein, Stratège digital**
