---
title: "Juliette Et Chocolat"
date: 2019-04-26T01:13:29-04:00
---
"J’ai eu la chance de faire connaissance avec Tran a travers les réseaux sociaux et quelle belle découverte! Ses photos culinaires sont à couper le souffle autant par ses cadrages, que sa luminosité et ses mises en scène! J’adore mes produits, mais je les ai redécouverts à travers les photos de Trang ! Une merveille!"

**Juliette et Chocolat**
