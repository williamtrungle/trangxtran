---
title: Scholar
date: "2019-01-03T01:16:02-04:00"
banner: schoolsection
social: [researchgate, journal, bookidote]
---
After finishing my major in Psychology and minor in Neurosciences, I now attempt to survive through medschool with the intent of learning even more about what surrounds us and what's inside us. Even though medicine is perceived as the most impactful field in science, I still believe in the value of maintaining a strong focus in research. That is why I am also involved in research projects and successfully published several scientific papers.
