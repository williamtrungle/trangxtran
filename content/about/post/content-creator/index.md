---
title: Content Creator
date: "2019-01-01T01:19:11-04:00"
banner: lobster
social: [instagram, facebook, wordpress]
---
What does it mean to be a content creator? It’s using the tools you have to create a pleasing and aesthetic content for the public. The content has to contribute to a a platform with a specific target community. My motto is simple: I blog what I’m passionate about. This is how I choose my niche and started my book blog by reviewing books, because I always loved to read, to posting about my food photography journey on Instagram, because who doesn’t like eating good food?
