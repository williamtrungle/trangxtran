---
banner: ramen
date: "2019-01-02T01:19:11-04:00"
title: Food Photographer
social: [instagram]
---
Food photography is one of the aspects of my life that came very naturally. As I try to better myself every day, food photography has become, slowly but surely, one of my favourite hobbies. It’s about creating and exposing my personal style through a lens. It’s about showing how I see the world around me and make it look interesting and aesthetic.
