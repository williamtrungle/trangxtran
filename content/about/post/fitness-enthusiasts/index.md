---
title: Fitness Enthusiast
date: "2019-01-04T01:19:04-04:00"
banner: gymsection
social: [couplegainz]
---
I have always struggled with discipline and one of the reasons why I love powerlifting is that the discipline and strength developed results in a healthy body and mind. It makes me feel more confident and forces me to do better than yesterday.
